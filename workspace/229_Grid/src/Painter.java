
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Dimension;
import java.awt.Point;
 
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;
 
 
public class Painter extends JPanel {
 
	static int gridWidth = 20;
	static int cellWidth = 35;
	static int edgeBuffer = 10;
	static Dimension windowSize = new Dimension(1280,720);
	
	static JFrame frame;
	
	public static class cell
	{
		public int x;
		public int y;
	}
	
	public static cell grid[][];
	
	public void paint(Graphics g) 
	{
		Graphics2D g2 = (Graphics2D)g;
		
		int odd = 1;
		
		for(int i = 0; i <= gridWidth; i++)
		{	
			for(int j = 0; j <= gridWidth; j++)
			{			
				if (odd == 1)
				{	
					if (j != gridWidth && i != gridWidth)
					{
						g2.setColor(Color.blue);
						g2.fillRect(grid[i][j].x, grid[i][j].y, cellWidth, cellWidth);
					}					
					odd = 0;
				}
				else
				{
					if (j != gridWidth && i != gridWidth)
					{
						g2.setColor(Color.green);
						g2.fillRect(grid[i][j].x, grid[i][j].y, cellWidth, cellWidth);
					}
					odd = 1;
				}	
			}
		}
		
		Point mousePos = frame.getContentPane().getMousePosition();
		
		if (mousePos != null)
		{
			int xLoc = (int)((((float)mousePos.y - (float)edgeBuffer) / (float)(frame.getContentPane().getHeight() - (2*edgeBuffer)))*gridWidth);
			int yLoc = (int)((((float)mousePos.x - (float)edgeBuffer) / (float)(frame.getContentPane().getHeight() - (2*edgeBuffer)))*gridWidth);
			
			if (xLoc >= gridWidth)
			{
				xLoc = gridWidth - 1;
			}
			if (yLoc >= gridWidth)
			{
				yLoc = gridWidth - 1;
			}
			
			g2.setColor(Color.gray);	
			g2.fillRect(grid[xLoc][yLoc].x, grid[xLoc][yLoc].y, cellWidth, cellWidth);
		}		
	}
 
	public static void main(String [] args) 
	{
		frame = new JFrame("Grid Highlighter 2000");
		
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		
		grid = new cell[gridWidth][gridWidth];
		
		for(int i = 0; i < gridWidth; i++)
		{
			for(int j = 0; j < gridWidth; j++)
			{
				cell newCell = new cell();
				
				newCell.x = edgeBuffer + (cellWidth * j);
				newCell.y = edgeBuffer + (cellWidth * i);
				
				grid[i][j] = newCell;
			}
		}
		
		Painter p = new Painter();
		
		frame.add(p);
		frame.getContentPane().setPreferredSize(windowSize);
		frame.pack();
		frame.setVisible(true);
		
		while(true)
		{
			p.repaint();
		}
	}
 
}